/**
 * Copyright (c) 2022 DB Netz AG and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package org.eclipse.set.model.tablemodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cell Content</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Content of a table cell.
 * <!-- end-model-doc -->
 *
 *
 * @see org.eclipse.set.model.tablemodel.TablemodelPackage#getCellContent()
 * @model abstract="true"
 * @generated
 */
public interface CellContent extends EObject {
} // CellContent
