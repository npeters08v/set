/**
 * Copyright (c) 2016 DB Netz AG and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package org.eclipse.set.feature.table.messages;

/**
 * Translations.
 * 
 * @author Schaefer
 */
public class Messages {

	/**
	 * Überprüfe Tabellen...
	 */
	public String TableOverviewPart_CalculateMissingTask;

	/**
	 * Tabelle wird erzeugt
	 */
	public String Abstracttableview_transformation_progress;

	/**
	 * Anhang
	 */
	public String Common_Attachment;

	/**
	 * x
	 */
	public String Common_Checkmark;

	/**
	 * Bezeichnung
	 */
	public String Common_Identifier;

	/**
	 * nein
	 */
	public String Common_No;

	/**
	 * keine angegeben
	 */
	public String Common_NotSpecified;

	/**
	 * Signalanordnung %s
	 */
	public String Common_PatternSignalArrangement;

	/**
	 * Für Bestand relevant
	 */
	public String Common_RelevantForInventory;

	/**
	 * Bemerkung
	 */
	public String Common_Remark;

	/**
	 * Hz
	 */
	public String Common_UnitHerz;

	/**
	 * 00/0
	 */
	public String Common_UnitIncline;

	/**
	 * kHz
	 */
	public String Common_UnitKiloherz;

	/**
	 * km
	 */
	public String Common_UnitKilometer;

	/**
	 * km/h
	 */
	public String Common_UnitKmh;

	/**
	 * km/h (m)
	 */
	public String Common_UnitKmh_m;

	/**
	 * m
	 */
	public String Common_UnitMeter;

	/**
	 * mm
	 */
	public String Common_UnitMillimeter;

	/**
	 * Ω*m
	 */
	public String Common_UnitOhm_m;

	/**
	 * %
	 */
	public String Common_UnitPercent;

	/**
	 * St
	 */
	public String Common_UnitPiece;

	/**
	 * s
	 */
	public String Common_UnitSeconds;

	/**
	 * ja
	 */
	public String Common_Yes;

	/**
	 * Durchrutschwegtabelle (alt)
	 */
	public String DwtTableView_Heading;

	/**
	 * Auflösung
	 */
	public String DwtTableView_HeadingAufloesung;

	/**
	 * Taste DA 4)
	 */
	public String DwtTableView_HeadingAufloesungTasteDa;

	/**
	 * verzögert [s] 3)
	 */
	public String DwtTableView_HeadingAufloesungVerzoegert;

	/**
	 * Zielgleisabschnitt
	 */
	public String DwtTableView_HeadingAufloesungZielgleisabschnitt;

	/**
	 * Länge [m]
	 */
	public String DwtTableView_HeadingAufloesungZielgleisabschnittLaenge;

	/**
	 * 
	 */
	public String DwtTableView_HeadingAufloesungZielgleisabschnittLeer;

	/**
	 * Besonderheiten
	 */
	public String DwtTableView_HeadingBesonderheiten;

	/**
	 * maßgebende Neigung [‰]
	 */
	public String DwtTableView_HeadingDecisiveTilt;

	/**
	 * Durchrutschweg
	 */
	public String DwtTableView_HeadingDweg;

	/**
	 * Bezeichnung 2)
	 */
	public String DwtTableView_HeadingDwegBezeichnung;

	/**
	 * bis
	 */
	public String DwtTableView_HeadingDwegBis;

	/**
	 * Länge [m] 1)
	 */
	public String DwtTableView_HeadingDwegLaenge;

	/**
	 * Ist
	 */
	public String DwtTableView_HeadingDwegLaengeIst;

	/**
	 * Soll
	 */
	public String DwtTableView_HeadingDwegLaengeSoll;

	/**
	 * v Einfahrt [km/h]
	 */
	public String DwtTableView_HeadingDwegVEinfahrt;

	/**
	 * genutzt
	 */
	public String DwtTableView_HeadingDwegVEinfahrtGenutzt;

	/**
	 * möglich
	 */
	public String DwtTableView_HeadingDwegVEinfahrtMoeglich;

	/**
	 * von Signal
	 */
	public String DwtTableView_HeadingDwegVonSignal;

	/**
	 * GUID
	 */
	public String DwtTableView_HeadingGuid;

	/**
	 * technisch freizuprüfende Freimeldeabschnitte
	 */
	public String DwtTableView_HeadingTechnischFreizupruefendeFreimeldeabschnitte;

	/**
	 * Weichen, Kreuzungen, Gleissperren
	 */
	public String DwtTableView_HeadingWkg;

	/**
	 * nicht verschließen 5) mit Flankenschutz
	 */
	public String DwtTableView_HeadingWkgNvmflks;

	/**
	 * nicht verschließen 5) ohne Flankenschutz
	 */
	public String DwtTableView_HeadingWkgNvoflks;

	/**
	 * verschließen mit Flankenschutz
	 */
	public String DwtTableView_HeadingWkgVmflks;

	/**
	 * verschließen ohne Flankenschutz
	 */
	public String DwtTableView_HeadingWkgVoflks;

	/**
	 * zulässige D-Weg-Expansion
	 */
	public String DwtTableView_HeadingZulaessigeDwegExpansion;

	/**
	 * Gefahrpunkttabelle (alt)
	 */
	public String GptTableView_Heading;

	/**
	 * Bemerkungen
	 */
	public String GptTableView_HeadingComment;

	/**
	 * maßgebende Neigung
	 */
	public String GptTableView_HeadingDecisiveTilt;

	/**
	 * Gefahrpunktabstand
	 */
	public String GptTableView_HeadingGefahrpunktabstand;

	/**
	 * Massgebender Gefahrpunkt
	 */
	public String GptTableView_HeadingGefahrpunktabstandGefahrpunkt;

	/**
	 * Länge [m]
	 */
	public String GptTableView_HeadingGefahrpunktabstandLaenge;

	/**
	 * Länge Ist
	 */
	public String GptTableView_HeadingGefahrpunktabstandLaengeIst;

	/**
	 * Länge Soll
	 */
	public String GptTableView_HeadingGefahrpunktabstandLaengeSoll;

	/**
	 * Signal
	 */
	public String GptTableView_HeadingGefahrpunktabstandSignal;

	/**
	 * R
	 */
	public String RatTableView_FstrArtR;

	/**
	 * U
	 */
	public String RatTableView_FstrArtU;

	/**
	 * Tabelle der aneinandergereihten Rangierfahrstaßen (RAT)
	 */
	public String RatTableView_Heading;

	/**
	 * nach Zielsignal/Zielgleis
	 */
	public String RatTableView_HeadingNachZielsignalZielgleis;

	/**
	 * Rangierstraßen
	 */
	public String RatTableView_HeadingRangierstrassen;

	/**
	 * Bemerkungen
	 */
	public String RatTableView_HeadingRemarks;

	/**
	 * Unterwegssignale
	 */
	public String RatTableView_HeadingUnterwegssignale;

	/**
	 * von Startsignal
	 */
	public String RatTableView_HeadingVonStartsignal;

	/**
	 * R
	 */
	public String RstTableView_FstrArtR;

	/**
	 * U
	 */
	public String RstTableView_FstrArtU;

	/**
	 * Rangierfahrstaßentabelle (alt)
	 */
	public String RstTableView_Heading;

	/**
	 * Abhängigkeit zu einer BÜ-Anlage
	 */
	public String RstTableView_HeadingDependencyLevelCrossing;

	/**
	 * Verzicht auf Zielgleisfreiprüfung
	 */
	public String RstTableView_HeadingDestinationTrackReleaseCheckWaiver;

	/**
	 * Ausschluss von Gegenfahrten
	 */
	public String RstTableView_HeadingExclusionOppositeRide;

	/**
	 * Ssp in der Rangierstraße - verschlossen und überwacht
	 */
	public String RstTableView_HeadingKeyLockLockedGuarded;

	/**
	 * Entscheidungs-Weichen
	 */
	public String RstTableView_HeadingMarshallingSwitches;

	/**
	 * leer
	 */
	public String RstTableView_HeadingMarshallingSwitchesEmpty;

	/**
	 * Lage L/R
	 */
	public String RstTableView_HeadingMarshallingSwitchesSwitchingPosition;

	/**
	 * Nicht überspannter Bereich
	 */
	public String RstTableView_HeadingNotSpannedSection;

	/**
	 * Start - Ziel
	 */
	public String RstTableView_HeadingOriginDestination;

	/**
	 * Durchlaufende Restrangierstraßenauflösung
	 */
	public String RstTableView_HeadingPassingThroughRemainderRelease;

	/**
	 * Punktförmige Gleisfreimeldeprüfung erforderlich
	 */
	public String RstTableView_HeadingPunctateTrackReleaseSignalCheckRequired;

	/**
	 * 
	 */
	public String RstTableView_HeadingRemarks;

	/**
	 * Rangierstraßenziel erlaubnisabhängig
	 */
	public String RstTableView_HeadingShuntingRouteDestinationPermissionDependent;

	/**
	 * Rangierstraßenzielsperre am Zielsignal
	 */
	public String RstTableView_HeadingShuntingRouteLockDestinationSignal;

	/**
	 * Rangierstraße mit Flankenschutz
	 */
	public String RstTableView_HeadingShuntingRouteWithLateralProtection;

	/**
	 * Rangierstraße ohne Zielsignal
	 */
	public String RstTableView_HeadingShuntingRouteWithoutDestinationSignal;

	/**
	 * Rangierstraße ohne Startsignal
	 */
	public String RstTableView_HeadingShuntingRouteWithoutStartSignal;

	/**
	 * Art (R/U)
	 */
	public String RstTableView_HeadingType;

	/**
	 * Bezeichner
	 */
	public String Site_Bezeichner;

	/**
	 * Drehung
	 */
	public String Site_Drehung;

	/**
	 * GUID
	 */
	public String Site_Guid;

	/**
	 * Orte (Objekte mit Koordinaten)
	 */
	public String Site_Heading;

	/**
	 * Koordinaten
	 */
	public String Site_Koordinaten;

	/**
	 * Koordinatensystem
	 */
	public String Site_Koordinatensystem;

	/**
	 * Typ
	 */
	public String Site_Typ;

	/**
	 * Orte (Objekte mit Koordinaten)
	 */
	public String SiteDescriptionService_ViewName;

	/**
	 * Objekte mit Koordinaten
	 */
	public String SiteDescriptionService_ViewTooltip;

	/**
	 * Sska (Elementansteuertabelle)
	 */
	public String SskaDescriptionService_ViewName;

	/**
	 * Elementansteuertabelle
	 */
	public String SskaDescriptionService_ViewTooltip;

	/**
	 * Bemerkung
	 */
	public String SskaTableView_Bemerkung;

	/**
	 * Grundsatzangaben
	 */
	public String SskaTableView_Grundsatzangaben;

	/**
	 * Grundsatzangaben Art
	 */
	public String SskaTableView_Grundsatzangaben_Art;

	/**
	 * Grundsatzangaben Bauart
	 */
	public String SskaTableView_Grundsatzangaben_Bauart;

	/**
	 * Grundsatzangaben
	 */
	public String SskaTableView_Grundsatzangaben_Bezeichnung_AEA_ASTW_Zentraleinheit;

	/**
	 * Grundsatzangaben Unterbringung
	 */
	public String SskaTableView_Grundsatzangaben_Unterbringung;

	/**
	 * Grundsatzangaben Unterbringung Art
	 */
	public String SskaTableView_Grundsatzangaben_Unterbringung_Art;

	/**
	 * Grundsatzangaben Unterbringung km
	 */
	public String SskaTableView_Grundsatzangaben_Unterbringung_km;

	/**
	 * Grundsatzangaben Unterbringung Ort
	 */
	public String SskaTableView_Grundsatzangaben_Unterbringung_Ort;

	/**
	 * Grundsatzangaben Unterbringung Strecke
	 */
	public String SskaTableView_Grundsatzangaben_Unterbringung_Strecke;

	/**
	 * Elementansteuertabelle (Sska)
	 */
	public String SskaTableView_Heading;

	/**
	 * IP-Adressangaben (DSTW)
	 */
	public String SskaTableView_IP_Adressangaben;

	/**
	 * Adressblock Blau
	 */
	public String SskaTableView_IP_Adressangaben_Adressblock_Blau;

	/**
	 * IPv4
	 */
	public String SskaTableView_IP_Adressangaben_Adressblock_Blau_IPv4_Blau;

	/**
	 * IPv4
	 */
	public String SskaTableView_IP_Adressangaben_Adressblock_Blau_IPv4_Grau;

	/**
	 * IPv6
	 */
	public String SskaTableView_IP_Adressangaben_Adressblock_Blau_IPv6_Blau;

	/**
	 * IPv6
	 */
	public String SskaTableView_IP_Adressangaben_Adressblock_Blau_IPv6_Grau;

	/**
	 * Adressblock Grau
	 */
	public String SskaTableView_IP_Adressangaben_Adressblock_Grau;

	/**
	 * GFK-Kategorie
	 */
	public String SskaTableView_IP_Adressangaben_GFK_Kategorie;

	/**
	 * Regionalbereich
	 */
	public String SskaTableView_IP_Adressangaben_Regionalbereich;

	/**
	 * Verknuepfungen
	 */
	public String SskaTableView_Verknuepfungen;

	/**
	 * Verknuepfungen Bedienung
	 */
	public String SskaTableView_Verknuepfungen_Bedienung;

	/**
	 * Verknuepfungen Bedienung lokal
	 */
	public String SskaTableView_Verknuepfungen_Bedienung_lokal;

	/**
	 * Verknuepfungen Bedienung Not-BP
	 */
	public String SskaTableView_Verknuepfungen_Bedienung_Not_BP;

	/**
	 * Verknuepfungen Bedienung Zentrale
	 */
	public String SskaTableView_Verknuepfungen_Bedienung_Zentrale;

	/**
	 * Verknuepfungen Bedienung bezirk
	 */
	public String SskaTableView_Verknuepfungen_Bedienung_Bezirk;

	/**
	 * Verknuepfungen Energie
	 */
	public String SskaTableView_Verknuepfungen_Energie;

	/**
	 * Verknuepfungen Energie primaer
	 */
	public String SskaTableView_Verknuepfungen_Energie_primaer;

	/**
	 * Verknuepfungen Energie sekundaer
	 */
	public String SskaTableView_Verknuepfungen_Energie_sekundaer;

	/**
	 * Verknuepfungen Information
	 */
	public String SskaTableView_Verknuepfungen_Information;

	/**
	 * Verknuepfungen Information primaer
	 */
	public String SskaTableView_Verknuepfungen_Information_primaer;

	/**
	 * Verknuepfungen Information sekundaer
	 */
	public String SskaTableView_Verknuepfungen_Information_sekundaer;

	/**
	 * Ssit (Bedieneinrichtungstabelle (Stw))
	 */
	public String SsitDescriptionService_ViewName;

	/**
	 * Bedieneinrichtungstabelle (Stw)
	 */
	public String SsitDescriptionService_ViewTooltip;

	/**
	 * Bedien- und Anzeigeelemente
	 */
	public String SsitTableView_Bedien_Anz_Elemente;

	/**
	 * Melder
	 */
	public String SsitTableView_Bedien_Anz_Elemente_Melder;

	/**
	 * Anschaltzeit Hupe
	 */
	public String SsitTableView_Bedien_Anz_Elemente_Nahbedienbereich_An_Zeit_Hupe;

	/**
	 * Anforderung NB
	 */
	public String SsitTableView_Bedien_Anz_Elemente_Nahbedienbereich_Anf_NB;

	/**
	 * Fertigmeldung
	 */
	public String SsitTableView_Bedien_Anz_Elemente_Nahbedienbereich_Fertigmeldung;

	/**
	 * Freigabe Schlüsselsperre
	 */
	public String SsitTableView_Bedien_Anz_Elemente_Nahbedienbereich_Freigabe_Ssp;

	/**
	 * Umstellung Gleissperre
	 */
	public String SsitTableView_Bedien_Anz_Elemente_Nahbedienbereich_Umst_Gs;

	/**
	 * Umstellung Signal
	 */
	public String SsitTableView_Bedien_Anz_Elemente_Nahbedienbereich_Umst_Sig;

	/**
	 * Umstellung Weiche
	 */
	public String SsitTableView_Bedien_Anz_Elemente_Nahbedienbereich_Umst_Weiche;

	/**
	 * Weichengruppe
	 */
	public String SsitTableView_Bedien_Anz_Elemente_Nahbedienbereich_Weichengruppe;

	/**
	 * Schalter
	 */
	public String SsitTableView_Bedien_Anz_Elemente_Schalter;

	/**
	 * Taste
	 */
	public String SsitTableView_Bedien_Anz_Elemente_Taste;

	/**
	 * Befestigung
	 */
	public String SsitTableView_Befestigung;

	/**
	 * Grundsatzangaben
	 */
	public String SsitTableView_Grundsatzangaben;

	/**
	 * Bauart
	 */
	public String SsitTableView_Grundsatzangaben_Bauart;

	/**
	 * Art
	 */
	public String SsitTableView_Grundsatzangaben_Befestigung_Art;

	/**
	 * km
	 */
	public String SsitTableView_Grundsatzangaben_Befestigung_km;

	/**
	 * Strecke
	 */
	public String SsitTableView_Grundsatzangaben_Befestigung_Strecke;

	/**
	 * Bezeichnung
	 */
	public String SsitTableView_Grundsatzangaben_Bezeichnung;

	/**
	 * Zugehörige Außenelementansteuerung
	 */
	public String SsitTableView_Grundsatzangaben_Zug_AEA;

	/**
	 * Bedieneinrichtungstabelle (Stw)
	 */
	public String SsitTableView_Heading;

	/**
	 * Nahstellbereich
	 */
	public String SsitTableView_Nahstellbereich;

	/**
	 * Weiche, Kreuzung, Gleissperre
	 */
	public String Sskf_Sonstiges_Weiche;

	/**
	 * Sskf (Freimeldetabelle)
	 */
	public String SskfDescriptionService_ViewName;

	/**
	 * Freimeldetabelle
	 */
	public String SskfDescriptionService_ViewTooltip;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Auswertung;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Auswertung_AeA;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Auswertung_Uebertragung;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Auswertung_Uebertragung_nach;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Auswertung_Uebertragung_Typ;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Auswertung_Uebertragung_von;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Bemerkung;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Ftgs;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Ftgs_Laenge;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Ftgs_Laenge_l1;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Ftgs_Laenge_l1_mit;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Ftgs_Laenge_l2;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Ftgs_Laenge_l2_mit;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Ftgs_Laenge_l3;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Ftgs_Laenge_l3_mit;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Ftgs_Laenge_ls;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Ftgs_Laenge_ls_mit;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Grundsatzangaben;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Grundsatzangaben_Art;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Grundsatzangaben_Bezeichnung;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Grundsatzangaben_Teilabschnitt;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Grundsatzangaben_Teilabschnitt_TA_Bez;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Grundsatzangaben_Teilabschnitt_TA_E_A;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Grundsatzangaben_Typ;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Heading;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_NfTf_GSK;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_NfTf_GSK_Laenge;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_NfTf_GSK_Laenge_beeinfl;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_NfTf_GSK_Laenge_beeinfl_mit;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_NfTf_GSK_Laenge_elektr;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_NfTf_GSK_Laenge_elektr_mit;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Sonstiges;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Sonstiges_Funktion;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Sonstiges_HFmeldung;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Sonstiges_OlA;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Sonstiges_OlA_Bezeichner;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Sonstiges_OlA_Schaltgruppe;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Sonstiges_Rbmin;

	/**
	 * Freimeldetabelle (Sskf)
	 */
	public String SskfTableView_Sonstiges_Rbmin_mit;

	/**
	 * Sskg (Gleisschaltmitteltabelle)
	 */
	public String SskgDescriptionService_ViewName;

	/**
	 * Gleisschaltmitteltabelle
	 */
	public String SskgDescriptionService_ViewTooltip;

	/**
	 * Achszählpunkte
	 */
	public String SskgTableView_Achszaehlpunkte;

	/**
	 * Achszählrechner AEA
	 */
	public String SskgTableView_Achszaehlpunkte_Anschluss_AEA;

	/**
	 * Anschluss Energieversorgung
	 */
	public String SskgTableView_Achszaehlpunkte_Anschluss_Energieversorgung;

	/**
	 * Energieversorgung AEA
	 */
	public String SskgTableView_Achszaehlpunkte_Anschluss_Energieversorgung_AEA;

	/**
	 * Anschluss Achszählrechner
	 */
	public String SskgTableView_Achszaehlpunkte_Anschluss_Rechner;

	/**
	 * separate Adern
	 */
	public String SskgTableView_Achszaehlpunkte_Anschluss_separateAdern;

	/**
	 * Schienenprofil
	 */
	public String SskgTableView_Achszaehlpunkte_Schienenprofil;

	/**
	 * Bemerkung
	 */
	public String SskgTableView_Bemerkung;

	/**
	 * Funktion
	 */
	public String SskgTableView_Funktion;

	/**
	 * Grundsatzangaben
	 */
	public String SskgTableView_Grundsatzangaben;

	/**
	 * Art
	 */
	public String SskgTableView_Grundsatzangaben_Art;

	/**
	 * Bezeichnung Gleisschaltmittel
	 */
	public String SskgTableView_Grundsatzangaben_Bezeichnung;

	/**
	 * Typ
	 */
	public String SskgTableView_Grundsatzangaben_Typ;

	/**
	 * Gleisschaltmitteltabelle
	 */
	public String SskgTableView_Heading;

	/**
	 * Standortmerkmale
	 */
	public String SskgTableView_Standortmerkmale;

	/**
	 * Bezugspunkt
	 */
	public String SskgTableView_Standortmerkmale_Bezugspunkt;

	/**
	 * Abstand
	 */
	public String SskgTableView_Standortmerkmale_Bezugspunkt_Abstand;

	/**
	 * m
	 */
	public String SskgTableView_Standortmerkmale_Bezugspunkt_Abstand_m;

	/**
	 * Bezeichnung
	 */
	public String SskgTableView_Standortmerkmale_Bezugspunkt_Bezeichnung;

	/**
	 * Standort
	 */
	public String SskgTableView_Standortmerkmale_Standort;

	/**
	 * km
	 */
	public String SskgTableView_Standortmerkmale_Standort_km;

	/**
	 * Strecke
	 */
	public String SskgTableView_Standortmerkmale_Standort_Strecke;

	/**
	 * Ssko (Schlosstabelle)
	 */
	public String SskoDescriptionService_ViewName;

	/**
	 * Schlosstabelle
	 */
	public String SskoDescriptionService_ViewTooltip;

	/**
	 * Grundsatzangaben
	 */
	public String SskoTableView_Grundsatzangaben;

	/**
	 * Bezeichnung Schloss
	 */
	public String SskoTableView_Grundsatzangaben_Bezeichnung;

	/**
	 * In Grundstellung eingeschlossen
	 */
	public String SskoTableView_Grundsatzangaben_InGrundstellung;

	/**
	 * Schloss an
	 */
	public String SskoTableView_Grundsatzangaben_Schlossan;

	/**
	 * Schlosstabelle
	 */
	public String SskoTableView_Heading;

	/**
	 * Schlosskombination/Schlüsselsperre
	 */
	public String SskoTableView_SchlosskombiSchluesselsperre;

	/**
	 * Bezeichnung
	 */
	public String SskoTableView_SchlosskombiSchluesselsperre_Bezeichnung;

	/**
	 * Hauptschloss
	 */
	public String SskoTableView_SchlosskombiSchluesselsperre_Hauptschloss;

	/**
	 * Unterbringung
	 */
	public String SskoTableView_SchlosskombiSchluesselsperre_Unterbringung;

	/**
	 * Art
	 */
	public String SskoTableView_SchlosskombiSchluesselsperre_Unterbringung_Art;

	/**
	 * km
	 */
	public String SskoTableView_SchlosskombiSchluesselsperre_Unterbringung_km;

	/**
	 * Ort
	 */
	public String SskoTableView_SchlosskombiSchluesselsperre_Unterbringung_Ort;

	/**
	 * Strecke
	 */
	public String SskoTableView_SchlosskombiSchluesselsperre_Unterbringung_Strecke;

	/**
	 * Schlüssel
	 */
	public String SskoTableView_Schluessel;

	/**
	 * Bartform
	 */
	public String SskoTableView_Schluessel_Bartform;

	/**
	 * Bezeichnung
	 */
	public String SskoTableView_Schluessel_Bezeichnung;

	/**
	 * Gruppe
	 */
	public String SskoTableView_Schluessel_Gruppe;

	/**
	 * Sonderanlage
	 */
	public String SskoTableView_Sonderanlage;

	/**
	 * Technisch Berechtigter
	 */
	public String SskoTableView_Technisch_Berechtigter;

	/**
	 * Weiche/Gleissperre/BÜ
	 */
	public String SskoTableView_WeicheGspBue;

	/**
	 * Schlossart
	 */
	public String SskoTableView_WeicheGspBue_Schlossart;

	/**
	 * Verschlossenes Element
	 */
	public String SskoTableView_WeicheGspBue_VerschlossenesElement;

	/**
	 * Bezeichnung
	 */
	public String SskoTableView_WeicheGspBue_VerschlossenesElement_Bezeichnung;

	/**
	 * Komponente
	 */
	public String SskoTableView_WeicheGspBue_VerschlossenesElement_Komponente;

	/**
	 * Lage
	 */
	public String SskoTableView_WeicheGspBue_VerschlossenesElement_Lage;

	/**
	 * Fahrweg
	 */
	public String SskoTableView_Fahrweg;

	/**
	 * Bezeichnung
	 */
	public String SskoTableView_Fahrweg_Bezeichnung;

	/**
	 * Zug
	 */
	public String SskoTableView_Fahrweg_Bezeichnung_Zug;

	/**
	 * Rangier
	 */
	public String SskoTableView_Fahrweg_Bezeichnung_Rangier;

	/**
	 * Ssks (Signaltabelle)
	 */
	public String SsksDescriptionService_ViewName;

	/**
	 * Signaltabelle
	 */
	public String SsksDescriptionService_ViewTooltip;

	/**
	 * Signaltabelle (Ssks)
	 */
	public String SsksTableView_Heading;

	/**
	 * Anschluss
	 */
	public String SsksTableView_HeadingAnschluss;

	/**
	 * Dauerhaft Nachtschaltung
	 */
	public String SsksTableView_HeadingAnschlussDauerhaftNachtschaltung;

	/**
	 * Schaltkasten
	 */
	public String SsksTableView_HeadingAnschlussSchaltkasten;

	/**
	 * Bezeichnung
	 */
	public String SsksTableView_HeadingAnschlussSchaltkastenBezeichnung;

	/**
	 * Entfernung
	 */
	public String SsksTableView_HeadingAnschlussSchaltkastenEntfernung;

	/**
	 * separater Schaltkasten Steuerung
	 */
	public String SsksTableView_HeadingAnschlussSeparaterSchaltkasten;

	/**
	 * Bezeichnung
	 */
	public String SsksTableView_HeadingAnschlussSeparaterSchaltkastenBezeichnung;

	/**
	 * Bezeichnung Signal
	 */
	public String SsksTableView_HeadingBezeichnungSignal;

	/**
	 * konstruktive Merkmale
	 */
	public String SsksTableView_HeadingKonstruktivmerkmale;

	/**
	 * Anordnung
	 */
	public String SsksTableView_HeadingKonstruktivmerkmaleAnordnung;

	/**
	 * Befestigung
	 */
	public String SsksTableView_HeadingKonstruktivmerkmaleAnordnungBefestigung;

	/**
	 * Regelzeichnung Nr. (Bild)
	 */
	public String SsksTableView_HeadingKonstruktivmerkmaleAnordnungRegelzeichnung;

	/**
	 * Fundament
	 */
	public String SsksTableView_HeadingKonstruktivmerkmaleFundament;

	/**
	 * Hoehe u.SO
	 */
	public String SsksTableView_HeadingKonstruktivmerkmaleFundamentHoeheUSO;

	/**
	 * Regelzeichnung Nr. (Bild)
	 */
	public String SsksTableView_HeadingKonstruktivmerkmaleFundamentRegelzeichnung;

	/**
	 * Obere Lichtpunkthoehe
	 */
	public String SsksTableView_HeadingKonstruktivmerkmaleObereLichtpunkthoehe;

	/**
	 * Streuscheibe
	 */
	public String SsksTableView_HeadingKonstruktivmerkmaleStreuscheibe;

	/**
	 * Art
	 */
	public String SsksTableView_HeadingKonstruktivmerkmaleStreuscheibeArt;

	/**
	 * Stellung
	 */
	public String SsksTableView_HeadingKonstruktivmerkmaleStreuscheibeStellung;

	/**
	 * Mastschild
	 */
	public String SsksTableView_HeadingMastschild;

	/**
	 * Nachgeordnetes Signal
	 */
	public String SsksTableView_HeadingNachgeordnetesSignal;

	/**
	 * Bemerkung
	 */
	public String SsksTableView_HeadingRemark;

	/**
	 * Signal-Art
	 */
	public String SsksTableView_HeadingSignalArt;

	/**
	 * Fiktives Signal
	 */
	public String SsksTableView_HeadingSignalArtFiktivesSignal;

	/**
	 * Reales Signal
	 */
	public String SsksTableView_HeadingSignalArtRealesSignal;

	/**
	 * Signalisierung
	 */
	public String SsksTableView_HeadingSignalisierung;

	/**
	 * Signalbegriffe (Schirm)
	 */
	public String SsksTableView_HeadingSignalisierungSchirm;

	/**
	 * Hp, (Hl)
	 */
	public String SsksTableView_HeadingSignalisierungSchirmHpHl;

	/**
	 * Ks, (Vr)
	 */
	public String SsksTableView_HeadingSignalisierungSchirmKsVr;

	/**
	 * Ra/Sh
	 */
	public String SsksTableView_HeadingSignalisierungSchirmRaSh;

	/**
	 * Zl, Kl
	 */
	public String SsksTableView_HeadingSignalisierungSchirmZlKl;

	/**
	 * Zs
	 */
	public String SsksTableView_HeadingSignalisierungSchirmZs;

	/**
	 * Signalbegriffe (Zusatzanzeiger)
	 */
	public String SsksTableView_HeadingSignalisierungZusatzanzeiger;

	/**
	 * Kombination
	 */
	public String SsksTableView_HeadingSignalisierungZusatzanzeigerKombination;

	/**
	 * Zp
	 */
	public String SsksTableView_HeadingSignalisierungZusatzanzeigerZp;

	/**
	 * Zs
	 */
	public String SsksTableView_HeadingSignalisierungZusatzanzeigerZs;

	/**
	 * Zs 2
	 */
	public String SsksTableView_HeadingSignalisierungZusatzanzeigerZs2;

	/**
	 * Zs 2v
	 */
	public String SsksTableView_HeadingSignalisierungZusatzanzeigerZs2v;

	/**
	 * Zs 3
	 */
	public String SsksTableView_HeadingSignalisierungZusatzanzeigerZs3;

	/**
	 * Zs 3v
	 */
	public String SsksTableView_HeadingSignalisierungZusatzanzeigerZs3v;

	/**
	 * Sonstiges
	 */
	public String SsksTableView_HeadingSonstiges;

	/**
	 * Automatische Fahrtstellung
	 */
	public String SsksTableView_HeadingSonstigesAutomatischeFahrtstellung;

	/**
	 * Dunkelschaltung
	 */
	public String SsksTableView_HeadingSonstigesDunkelschaltung;

	/**
	 * Durchfahrt erlaubt
	 */
	public String SsksTableView_HeadingSonstigesDurchfahrtErlaubt;

	/**
	 * Löschung Zs 1/Zs 7
	 */
	public String SsksTableView_HeadingSonstigesLoeschungZs1Zs7;

	/**
	 * Signalbedienung wb/zb
	 */
	public String SsksTableView_HeadingSonstigesSignalbedienungWbZb;

	/**
	 * Überwachung
	 */
	public String SsksTableView_HeadingSonstigesUeberwachung;

	/**
	 * Zs 2
	 */
	public String SsksTableView_HeadingSonstigesUeberwachungZs2;

	/**
	 * Zs 2v
	 */
	public String SsksTableView_HeadingSonstigesUeberwachungZs2v;

	/**
	 * Standortmerkmale
	 */
	public String SsksTableView_HeadingStandortmerkmale;

	/**
	 * Abstand Gleismitte - Mastmitte
	 */
	public String SsksTableView_HeadingStandortmerkmaleAbstandGleismitteMastmitte;

	/**
	 * links
	 */
	public String SsksTableView_HeadingStandortmerkmaleAbstandGleismitteMastmitteLinks;

	/**
	 * rechts
	 */
	public String SsksTableView_HeadingStandortmerkmaleAbstandGleismitteMastmitteRechts;

	/**
	 * Ausrichtung
	 */
	public String SsksTableView_HeadingStandortmerkmaleAusrichtung;

	/**
	 * Entfernung
	 */
	public String SsksTableView_HeadingStandortmerkmaleAusrichtungEntfernung;

	/**
	 * Lichtraumprofil
	 */
	public String SsksTableView_HeadingStandortmerkmaleLichtraumprofil;

	/**
	 * Richtpunkt
	 */
	public String SsksTableView_HeadingStandortmerkmaleRichtpunkt;

	/**
	 * Sichtbarkeit
	 */
	public String SsksTableView_HeadingStandortmerkmaleSichtbarkeit;

	/**
	 * Ist
	 */
	public String SsksTableView_HeadingStandortmerkmaleSichtbarkeitIst;

	/**
	 * Mindest
	 */
	public String SsksTableView_HeadingStandortmerkmaleSichtbarkeitMindest;

	/**
	 * Soll
	 */
	public String SsksTableView_HeadingStandortmerkmaleSichtbarkeitSoll;

	/**
	 * Sonstige Zulässige Anordnung
	 */
	public String SsksTableView_HeadingStandortmerkmaleSonstigeZulaessigeAnordnung;

	/**
	 * Standort
	 */
	public String SsksTableView_HeadingStandortmerkmaleStandort;

	/**
	 * Strecke
	 */
	public String SsksTableView_HeadingStandortmerkmaleStrecke;

	/**
	 * Ueberhoehung
	 */
	public String SsksTableView_HeadingStandortmerkmaleUeberhoehung;

	/**
	 * Vorsignalbake
	 */
	public String SsksTableView_HeadingVorsignalbake;

	/**
	 * Anz.
	 */
	public String SsksTableView_HeadingVorsignalbakeAnz;

	/**
	 * Regelzeichnung Nr. (Bild)
	 */
	public String SsksTableView_HeadingVorsignalbakeRegelzeichnung;

	/**
	 * Vorsignaltafel
	 */
	public String SsksTableView_HeadingVorsignaltafel;

	/**
	 * Regelzeichnung Nr. (Bild)
	 */
	public String SsksTableView_HeadingVorsignaltafelRegelzeichnung;

	/**
	 * Besetzte Ausfahrt
	 */
	public String SsksTableView_Sonstiges_Besetzte_Ausfahrt;

	/**
	 * Blau
	 */
	public String Sskt_Adressblock_Blau;

	/**
	 * Grau
	 */
	public String Sskt_Adressblock_Grau;

	/**
	 * Grundsatzangaben
	 */
	public String Sskt_Grundsatzangaben;

	/**
	 * Art
	 */
	public String Sskt_Grundsatzangaben_Art;

	/**
	 * BSO
	 */
	public String Sskt_Grundsatzangaben_Bedien_Standort;

	/**
	 * Bezeichnung TSO/BSO
	 */
	public String Sskt_Grundsatzangaben_Bezeichnung;

	/**
	 * Art
	 */
	public String Sskt_Grundsatzangaben_Unterbringung_Art;

	/**
	 * km
	 */
	public String Sskt_Grundsatzangaben_Unterbringung_km;

	/**
	 * Ort
	 */
	public String Sskt_Grundsatzangaben_Unterbringung_Ort;

	/**
	 * Strecke
	 */
	public String Sskt_Grundsatzangaben_Unterbringung_Strecke;

	/**
	 * Tabelle der Technik- und Bedienstandorte (Sskt)
	 */
	public String Sskt_Heading;

	/**
	 * IP-Adressangaben
	 */
	public String Sskt_IP_Adressangaben;

	/**
	 * IPv4
	 */
	public String Sskt_IP_Adressangaben_Adressblock_Blau_IPv4_Blau;

	/**
	 * IPv6
	 */
	public String Sskt_IP_Adressangaben_Adressblock_Blau_IPv6_Blau;

	/**
	 * IPv4
	 */
	public String Sskt_IP_Adressangaben_Adressblock_Grau_IPv4_Grau;

	/**
	 * IPv6
	 */
	public String Sskt_IP_Adressangaben_Adressblock_Grau_IPv6_Grau;

	/**
	 * Regionalbereich
	 */
	public String Sskt_IP_Adressangaben_Regionalbereich;

	/**
	 * Art
	 */
	public String Sskt_IP_Adressangaben_Teilsystem_Art;

	/**
	 * Blau
	 */
	public String Sskt_IP_Adressangaben_Teilsystem_TS_Blau;

	/**
	 * Grau
	 */
	public String Sskt_IP_Adressangaben_Teilsystem_TS_Grau;

	/**
	 * Teilsystem
	 */
	public String Sskt_Teilsystem;

	/**
	 * Unterbringung
	 */
	public String Sskt_Unterbringung;

	/**
	 * Sskt (Tabelle der Technik- und Bedienstandorte)
	 */
	public String SsktDescriptionService_ViewName;

	/**
	 * Tabelle der Technik- und Bedienstandorte
	 */
	public String SsktDescriptionService_ViewTooltip;

	/**
	 * Signaltabelle Entwurf (Ssld)
	 */
	public String SsktTableView_Heading;

	/**
	 * Abstand Gleismitte
	 */
	public String SsktTableView_HeadingAbstandGleismitte;

	/**
	 * Befestigung
	 */
	public String SsktTableView_HeadingBefestigung;

	/**
	 * Bezeichnung Signal
	 */
	public String SsktTableView_HeadingBezeichnungSignal;

	/**
	 * Entfernung
	 */
	public String SsktTableView_HeadingEntfernung;

	/**
	 * Km
	 */
	public String SsktTableView_HeadingKm;

	/**
	 * Richtpunkt
	 */
	public String SsktTableView_HeadingRichtpunkt;

	/**
	 * Sichtbarkeit Ist
	 */
	public String SsktTableView_HeadingSichtbarkeitIst;

	/**
	 * Sichtbarkeit Mindest
	 */
	public String SsktTableView_HeadingSichtbarkeitMindest;

	/**
	 * Sichtbarkeit Soll
	 */
	public String SsktTableView_HeadingSichtbarkeitSoll;

	/**
	 * Strecke
	 */
	public String SsktTableView_HeadingStrecke;

	/**
	 * Sskw (Weichentabelle)
	 */
	public String SskwDescriptionService_ViewName;

	/**
	 * Weichentabelle
	 */
	public String SskwDescriptionService_ViewTooltip;

	/**
	 * Bemerkung
	 */
	public String SskwTableView_Bemerkung;

	/**
	 * Freimeldung
	 */
	public String SskwTableView_Freimeldung;

	/**
	 * FMA
	 */
	public String SskwTableView_Freimeldung_Fma;

	/**
	 * Isolierfall
	 */
	public String SskwTableView_Freimeldung_Isolierfall;

	/**
	 * nicht grenzzeichenfrei
	 */
	public String SskwTableView_Freimeldung_nicht_grenzzeichenfrei;

	/**
	 * Links mit
	 */
	public String SskwTableView_Freimeldung_nicht_grenzzeichenfrei_Links;

	/**
	 * Rechts mit
	 */
	public String SskwTableView_Freimeldung_nicht_grenzzeichenfrei_Rechts;

	/**
	 * Gleissperre
	 */
	public String SskwTableView_Gleissperre;

	/**
	 * Anzahl Antriebe
	 */
	public String SskwTableView_Gleissperre_Antriebe;

	/**
	 * Auswurfrichtung
	 */
	public String SskwTableView_Gleissperre_Auswurfrichtung;

	/**
	 * Gsp-signal
	 */
	public String SskwTableView_Gleissperre_Gsp_signal;

	/**
	 * Schutzschiene
	 */
	public String SskwTableView_Gleissperre_Schutzschiene;

	/**
	 * Weichentabelle (Sskw)
	 */
	public String SskwTableView_Heading;

	/**
	 * Herzstück
	 */
	public String SskwTableView_Herzstueck;

	/**
	 * Anzahl Antriebe
	 */
	public String SskwTableView_Herzstueck_Antriebe;

	/**
	 * Kreuzung
	 */
	public String SskwTableView_Kreuzung;

	/**
	 * zul. Geschwindigkeit (Radius)
	 */
	public String SskwTableView_Kreuzung_v_zul_K;

	/**
	 * Links
	 */
	public String SskwTableView_Kreuzung_v_zul_K_Links;

	/**
	 * Rechts
	 */
	public String SskwTableView_Kreuzung_v_zul_K_Rechts;

	/**
	 * Sonderanlage
	 */
	public String SskwTableView_Sonderanlage;

	/**
	 * Art
	 */
	public String SskwTableView_Sonderanlage_Art;

	/**
	 * Sonstiges
	 */
	public String SskwTableView_Sonstiges;

	/**
	 * DWS
	 */
	public String SskwTableView_Sonstiges_DWs;

	/**
	 * Regelzeichnung Nr (Bild)
	 */
	public String SskwTableView_Sonstiges_Regelzeichnung_Nr;

	/**
	 * Vorzugslage/ Grundstellung
	 */
	public String SskwTableView_Vorzugslage;

	/**
	 * Automatik
	 */
	public String SskwTableView_Vorzugslage_Automatik;

	/**
	 * Lage/ Stellung
	 */
	public String SskwTableView_Vorzugslage_Lage;

	/**
	 * Weiche
	 */
	public String SskwTableView_Weiche;

	/**
	 * Anzahl Antriebe (Lage)
	 */
	public String SskwTableView_Weiche_Antriebe;

	/**
	 * Auffahrortung
	 */
	public String SskwTableView_Weiche_Auffahrortung;

	/**
	 * Bezeichnung
	 */
	public String SskwTableView_Weiche_Kreuzung_Gleissperre_Bezeichnung;

	/**
	 * Form
	 */
	public String SskwTableView_Weiche_Kreuzung_Gleissperre_Form;

	/**
	 * Weiche/Kreuzung/Gleissperre/Sonderanlage
	 */
	public String SskwTableView_Weiche_Kreuzung_Gleissperre_Sonderanlage;

	/**
	 * Prüfkontakte
	 */
	public String SskwTableView_Weiche_Pruefkontakte;

	/**
	 * zul. Geschwindigkeit (Radius)
	 */
	public String SskwTableView_Weiche_v_zul_W;

	/**
	 * Links
	 */
	public String SskwTableView_Weiche_v_zul_W_Links;

	/**
	 * Rechts
	 */
	public String SskwTableView_Weiche_v_zul_W_Rechts;

	/**
	 * Weichensignal
	 */
	public String SskwTableView_Weiche_Weichensignal;

	/**
	 * Ssla (Tabelle der aneinandergereihten Fahrstraßen)
	 */
	public String SslaDescriptionService_ViewName;

	/**
	 * Tabelle der aneinandergereihten Fahrstraßen
	 */
	public String SslaDescriptionService_ViewTooltip;

	/**
	 * Aneinandergereihte Fahrstraßen (Ssla)
	 */
	public String SslaTableView_Bemerkung;

	/**
	 * Aneinandergereihte Fahrstraßen (Ssla)
	 */
	public String SslaTableView_Grundsatzangaben;

	/**
	 * Aneinandergereihte Fahrstraßen (Ssla)
	 */
	public String SslaTableView_Grundsatzangaben_Art;

	/**
	 * Aneinandergereihte Fahrstraßen (Ssla)
	 */
	public String SslaTableView_Grundsatzangaben_Bezeichnung;

	/**
	 * Aneinandergereihte Fahrstraßen (Ssla)
	 */
	public String SslaTableView_Grundsatzangaben_Durchrutschweg_Ziel;

	/**
	 * Aneinandergereihte Fahrstraßen (Ssla)
	 */
	public String SslaTableView_Grundsatzangaben_Fahrweg;

	/**
	 * Aneinandergereihte Fahrstraßen (Ssla)
	 */
	public String SslaTableView_Grundsatzangaben_Fahrweg_Start;

	/**
	 * Aneinandergereihte Fahrstraßen (Ssla)
	 */
	public String SslaTableView_Grundsatzangaben_Fahrweg_Ziel;

	/**
	 * Aneinandergereihte Fahrstraßen (Ssla)
	 */
	public String SslaTableView_Heading;

	/**
	 * Aneinandergereihte Fahrstraßen (Ssla)
	 */
	public String SslaTableView_Unterwegssignal;

	/**
	 * Sslb (Streckenblocktabelle)
	 */
	public String SslbDescriptionService_ViewName;

	/**
	 * Streckenblocktabelle
	 */
	public String SslbDescriptionService_ViewTooltip;

	/**
	 * Abgabe-speicherung
	 */
	public String SslbTableView_Abgabespeicherung;

	/**
	 * Abhängigkeit Durchrutschweg/Rangierzielsperre
	 */
	public String SslbTableView_AbhaengigkeitDurchrutschwegRangierzielsperre;

	/**
	 * Akustische Meldung
	 */
	public String SslbTableView_AkustischeMeldung;

	/**
	 * Anordnung
	 */
	public String SslbTableView_Anordnung;

	/**
	 * Anrückabschnitt
	 */
	public String SslbTableView_Anrueckabschnitt;

	/**
	 * Awanst
	 */
	public String SslbTableView_Awanst;

	/**
	 * Bezeichnung (Bedienung von)
	 */
	public String SslbTableView_AwanstBezeichnungBedienungVon;

	/**
	 * Bauform
	 */
	public String SslbTableView_Bauform;

	/**
	 * Betriebsführung
	 */
	public String SslbTableView_Betriebsfuehrung;

	/**
	 * Betriebsstelle
	 */
	public String SslbTableView_Betriebsstelle;

	/**
	 * Bezeichnung
	 */
	public String SslbTableView_Bezeichnung;

	/**
	 * Blockmeldung
	 */
	public String SslbTableView_Blockmeldung;

	/**
	 * Blockschaltung
	 */
	public String SslbTableView_Blockschaltung;

	/**
	 * Erlaubnis
	 */
	public String SslbTableView_Erlaubnis;

	/**
	 * Gleis
	 */
	public String SslbTableView_Gleis;

	/**
	 * Grundsatzangaben
	 */
	public String SslbTableView_Grundsatzangaben;

	/**
	 * Sslb (Streckenblocktabelle)
	 */
	public String SslbTableView_Heading;

	/**
	 * Holen
	 */
	public String SslbTableView_Holen;

	/**
	 * Nach
	 */
	public String SslbTableView_Nach;

	/**
	 * Nummer
	 */
	public String SslbTableView_Nummer;

	/**
	 * Räumungsprüfung
	 */
	public String SslbTableView_Raeumungspruefung;

	/**
	 * Rückblockwecker
	 */
	public String SslbTableView_Rueckblockwecker;

	/**
	 * Rücklauf automatisch
	 */
	public String SslbTableView_RuecklaufAutomatisch;

	/**
	 * Schutzübertrager
	 */
	public String SslbTableView_Schutzuebertrager;

	/**
	 * Ständig vorhanden
	 */
	public String SslbTableView_StaendigVorhanden;

	/**
	 * Strecke
	 */
	public String SslbTableView_Strecke;

	/**
	 * Von
	 */
	public String SslbTableView_Von;

	/**
	 * Vorblockwecker
	 */
	public String SslbTableView_Vorblockwecker;

	/**
	 * Zugschlussmeldung
	 */
	public String SslbTableView_Zugschlussmeldung;

	/**
	 * Ssld (Durchrutschweg- und Gefahrpunkttabelle)
	 */
	public String SsldDescriptionService_ViewName;

	/**
	 * Durchrutschweg- und Gefahrpunkttabelle
	 */
	public String SsldDescriptionService_ViewTooltip;

	/**
	 * Durchrutschwegtabelle (Ssld)
	 */
	public String SsldTableView_Heading;

	/**
	 * Abhängigkeiten
	 */
	public String SsldTableView_HeadingDependencies;

	/**
	 * relevante FmA
	 */
	public String SsldTableView_HeadingDependenciesFma;

	/**
	 * Erlaubnisabhängig
	 */
	public String SsldTableView_HeadingDependenciesPermissionDependent;

	/**
	 * Weichen, Kreuzungen
	 */
	public String SsldTableView_HeadingDependenciesPointsCrossings;

	/**
	 * mit Verschluss
	 */
	public String SsldTableView_HeadingDependenciesPointsCrossingsLock;

	/**
	 * ohne Verschluss
	 */
	public String SsldTableView_HeadingDependenciesPointsCrossingsNoLock;

	/**
	 * v-Aufwertung Verzicht
	 */
	public String SsldTableView_HeadingDependenciesVWaiver;

	/**
	 * Grundsatzangaben
	 */
	public String SsldTableView_HeadingFundamental;

	/**
	 * Gefahrpunkt
	 */
	public String SsldTableView_HeadingFundamentalDangerousPoint;

	/**
	 * von (Signal)
	 */
	public String SsldTableView_HeadingFundamentalFrom;

	/**
	 * Bezeichnung
	 */
	public String SsldTableView_HeadingFundamentalIndication;

	/**
	 * PZB-Schutzpunkt
	 */
	public String SsldTableView_HeadingFundamentalProtectionPointPzb;

	/**
	 * bis (Markanter Punkt)
	 */
	public String SsldTableView_HeadingFundamentalTo;

	/**
	 * Eigenschaften
	 */
	public String SsldTableView_HeadingProperties;

	/**
	 * maßgebende Neigung
	 */
	public String SsldTableView_HeadingPropertiesDecisiveTilt;

	/**
	 * Frei-gemeldet
	 */
	public String SsldTableView_HeadingPropertiesFree;

	/**
	 * Länge [m]
	 */
	public String SsldTableView_HeadingPropertiesLength;

	/**
	 * Ist
	 */
	public String SsldTableView_HeadingPropertiesLengthActual;

	/**
	 * Soll
	 */
	public String SsldTableView_HeadingPropertiesLengthNominal;

	/**
	 * Zielgeschwindigkeit möglich
	 */
	public String SsldTableView_HeadingPropertiesTargetSpeedPossible;

	/**
	 * Bemerkung
	 */
	public String SsldTableView_HeadingRemark;

	/**
	 * Auflösung
	 */
	public String SsldTableView_HeadingUnblocking;

	/**
	 * Auflöseverzögerung bzw. Kennlichtverzögerungszeit
	 */
	public String SsldTableView_HeadingUnblockingDelay;

	/**
	 * Zielgleisabschnitt
	 */
	public String SsldTableView_HeadingUnblockingDestinationTrack;

	/**
	 * Zielgleisabschnitt_Bezeichnung
	 */
	public String SsldTableView_HeadingUnblockingDestinationTrackIndication;

	/**
	 * Länge
	 */
	public String SsldTableView_HeadingUnblockingDestinationTrackLength;

	/**
	 * manuell
	 */
	public String SsldTableView_HeadingUnblockingManual;

	/**
	 * Sslf (Flankenschutztabelle)
	 */
	public String SslfDescriptionService_ViewName;

	/**
	 * Flankenschutztabelle
	 */
	public String SslfDescriptionService_ViewTooltip;

	/**
	 * Bemerkung
	 */
	public String SslfTableView_Bemerkung;

	/**
	 * Flankenschutzanforderer
	 */
	public String SslfTableView_Flankenschutzanforderer;

	/**
	 * Bezeichnung Weiche/Nb
	 */
	public String SslfTableView_Flankenschutzanforderer_Bezeichnung_W_Nb;

	/**
	 * W-Lage/Nb-Grenze
	 */
	public String SslfTableView_Flankenschutzanforderer_WLage_NbGrenze;

	/**
	 * Flankenschutztabelle (Sslf)
	 */
	public String SslfTableView_Heading;

	/**
	 * Schutzraumüberwachung
	 */
	public String SslfTableView_Schutzraumueberwachung;

	/**
	 * freigemeldet
	 */
	public String SslfTableView_Schutzraumueberwachung_freigemeldet;

	/**
	 * nicht freigemeldet
	 */
	public String SslfTableView_Schutzraumueberwachung_nicht_freigemeldet;

	/**
	 * Technischer Verzicht
	 */
	public String SslfTableView_Technischer_Verzicht;

	/**
	 * Unmittelbarer Flankenschutz
	 */
	public String SslfTableView_Unmittelbarer_Flankenschutz;

	/**
	 * Signal
	 */
	public String SslfTableView_Unmittelbarer_Flankenschutz_Signal;

	/**
	 * Bezeichnung
	 */
	public String SslfTableView_Unmittelbarer_Flankenschutz_Signal_Bezeichnung_Sig;

	/**
	 * Rangierzielsperre
	 */
	public String SslfTableView_Unmittelbarer_Flankenschutz_Signal_Rangierzielsperre;

	/**
	 * Weiche/Gleissperre
	 */
	public String SslfTableView_Unmittelbarer_Flankenschutz_Weiche_Gleissperre;

	/**
	 * Bezeichnung
	 */
	public String SslfTableView_Unmittelbarer_Flankenschutz_Weiche_Gleissperre_Bezeichnung_W;

	/**
	 * Lage
	 */
	public String SslfTableView_Unmittelbarer_Flankenschutz_Weiche_Gleissperre_Lage;

	/**
	 * Zwieschutz
	 */
	public String SslfTableView_Unmittelbarer_Flankenschutz_Weiche_Gleissperre_Zwieschutz;

	/**
	 * Weitergabe
	 */
	public String SslfTableView_Weitergabe;

	/**
	 * Weiche/Kreuzung
	 */
	public String SslfTableView_Weitergabe_Weiche_Kreuzung;

	/**
	 * Bezeichnung
	 */
	public String SslfTableView_Weitergabe_Weiche_Kreuzung_Bezeichnung_W_Kr;

	/**
	 * wie Fahrt über
	 */
	public String SslfTableView_Weitergabe_Weiche_Kreuzung_wie_Fahrt_ueber;

	/**
	 * Zusätzlich bei EKW
	 */
	public String SslfTableView_Weitergabe_Zusaetzlich_EKW;

	/**
	 * Bezeichnung
	 */
	public String SslfTableView_Weitergabe_Zusaetzlich_EKW_Bezeichnung_W_Kr;

	/**
	 * wie Fahrt über
	 */
	public String SslfTableView_Weitergabe_Zusaetzlich_EKW_wie_Fahrt_ueber;

	/**
	 * Ausschluss gleichzeitiger Fahrten
	 */
	public String Ssli_Ausschluss_Fahrten;

	/**
	 * Rangierfahrt
	 */
	public String Ssli_Ausschluss_Fahrten_Rangierfahrt;

	/**
	 * Ausfahrt
	 */
	public String Ssli_Ausschluss_Fahrten_Rangierfahrt_Ausfahrt;

	/**
	 * Einfahrt
	 */
	public String Ssli_Ausschluss_Fahrten_Rangierfahrt_Einfahrt;

	/**
	 * Zugausfahrt
	 */
	public String Ssli_Ausschluss_Fahrten_Zugausfahrt;

	/**
	 * Grundsatzangaben
	 */
	public String Ssli_Grundsatzangaben;

	/**
	 * Begrenzende Signale
	 */
	public String Ssli_Grundsatzangaben_Begrenzende_Signale;

	/**
	 * N/X-Richtung
	 */
	public String Ssli_Grundsatzangaben_Begrenzende_Signale_NX_Richtung;

	/**
	 * P/Y-Richtung
	 */
	public String Ssli_Grundsatzangaben_Begrenzende_Signale_PY_Richtung;

	/**
	 * Bezeichnung Inselgleis
	 */
	public String Ssli_Grundsatzangaben_Bezeichnung_Inselgleis;

	/**
	 * Länge
	 */
	public String Ssli_Grundsatzangaben_Laenge;

	/**
	 * Inselgleistabelle (Ssli)
	 */
	public String Ssli_Heading;

	/**
	 * Ssli (Inselgleistabelle)
	 */
	public String SsliDescriptionService_ViewName;

	/**
	 * Inselgleistabelle
	 */
	public String SsliDescriptionService_ViewTooltip;

	/**
	 * Ssln (Nahbedientabelle)
	 */
	public String SslnDescriptionService_ViewName;

	/**
	 * Nahbedientabelle
	 */
	public String SslnDescriptionService_ViewTooltip;

	/**
	 * Element
	 */
	public String SslnTableView_Element;

	/**
	 * Bedieneinrichtung
	 */
	public String SslnTableView_Element_Bedien_Einr;

	/**
	 * Signal
	 */
	public String SslnTableView_Element_Signal;

	/**
	 * frei stellbar (Rückgabevoraussetzung)
	 */
	public String SslnTableView_Element_Signal_frei_stellbar;

	/**
	 * Kennlicht
	 */
	public String SslnTableView_Element_Signal_Kennlicht;

	/**
	 * Schlüsselsperre
	 */
	public String SslnTableView_Element_Ssp;

	/**
	 * Weiche/Gleissperre
	 */
	public String SslnTableView_Element_Weiche_Gs;

	/**
	 * frei stellbar (Rückgabevoraussetzung)
	 */
	public String SslnTableView_Element_Weiche_Gs_frei_stellbar;

	/**
	 * verschlossen
	 */
	public String SslnTableView_Element_Weiche_Gs_verschlossen;

	/**
	 * Grenze
	 */
	public String SslnTableView_Grenze;

	/**
	 * Bezeichnung (Element innen, außen)
	 */
	public String SslnTableView_Grenze_Bez_Grenze;

	/**
	 * Grundsatzangaben
	 */
	public String SslnTableView_Grundsatzangaben;

	/**
	 * Art
	 */
	public String SslnTableView_Grundsatzangaben_Art;

	/**
	 * Bereich/Zone
	 */
	public String SslnTableView_Grundsatzangaben_Bereich_Zone;

	/**
	 * Nahbedienungstabelle (Ssln)
	 */
	public String SslnTableView_Heading;

	/**
	 * Nahbedienbereich R
	 */
	public String SslnTableView_NB_R;

	/**
	 * Bedienungshandlung
	 */
	public String SslnTableView_Ssln_NB_R_Bedienungshandlung;

	/**
	 * Unterstellungsverhältnis
	 */
	public String SslnTableView_Unterstellungsverhaeltnis;

	/**
	 * Auflösung Grenze zu übergeordneter Zone
	 */
	public String SslnTableView_Unterstellungsverhaeltnis_Aufloesung_Grenze;

	/**
	 * Rang der Zuschaltung
	 */
	public String SslnTableView_Unterstellungsverhaeltnis_Rang_Zuschaltung;

	/**
	 * untergeordnet zu
	 */
	public String SslnTableView_Unterstellungsverhaeltnis_untergeordnet;

	/**
	 * Abhängigkeiten
	 */
	public String Sslr_Abhaengigkeiten;

	/**
	 * Abhängiger BÜ
	 */
	public String Sslr_Abhaengigkeiten_Abhaengiger_BUe;

	/**
	 * Auflösung nicht angefahrener Fahrstraßen
	 */
	public String Sslr_Abhaengigkeiten_Aufloes_Fstr;

	/**
	 * Fahrwegweichen mit Flankenschutz
	 */
	public String Sslr_Abhaengigkeiten_FwWeichen_mit_Fla;

	/**
	 * Gleisfreimeldung
	 */
	public String Sslr_Abhaengigkeiten_Gleisfreimeldung;

	/**
	 * Inselgleis
	 */
	public String Sslr_Abhaengigkeiten_Inselgleis;

	/**
	 * Bezeichnung
	 */
	public String Sslr_Abhaengigkeiten_Inselgleis_Bezeichnung;

	/**
	 * Gegenfahrtausschluss
	 */
	public String Sslr_Abhaengigkeiten_Inselgleis_Gegenfahrtausschluss;

	/**
	 * Überwachte Ssp
	 */
	public String Sslr_Abhaengigkeiten_Ueberwachte_Ssp;

	/**
	 * Ziel erlaubnisabhängig
	 */
	public String Sslr_Abhaengigkeiten_Ziel_erlaubnisabh;

	/**
	 * Einstellung
	 */
	public String Sslr_Einstellung;

	/**
	 * Autom. Einstellung
	 */
	public String Sslr_Einstellung_Autom_Einstellung;

	/**
	 * F-Bedienung
	 */
	public String Sslr_Einstellung_F_Bedienung;

	/**
	 * Grundsatzangaben
	 */
	public String Sslr_Grundsatzangaben;

	/**
	 * Art
	 */
	public String Sslr_Grundsatzangaben_Art;

	/**
	 * Bezeichnung
	 */
	public String Sslr_Grundsatzangaben_Bezeichnung;

	/**
	 * Fahrweg
	 */
	public String Sslr_Grundsatzangaben_Fahrweg;

	/**
	 * Entscheidungsweiche mit Stellung
	 */
	public String Sslr_Grundsatzangaben_Fahrweg_Entscheidungsweiche;

	/**
	 * Nr.
	 */
	public String Sslr_Grundsatzangaben_Fahrweg_Nummer;

	/**
	 * Start
	 */
	public String Sslr_Grundsatzangaben_Fahrweg_Start;

	/**
	 * Ziel
	 */
	public String Sslr_Grundsatzangaben_Fahrweg_Ziel;

	/**
	 * Rangierstraßentabelle (Sslr)
	 */
	public String Sslr_Heading;

	/**
	 * Sslr (Rangierstraßentabelle)
	 */
	public String SslrDescriptionService_ViewName;

	/**
	 * Rangierstraßentabelle
	 */
	public String SslrDescriptionService_ViewTooltip;

	/**
	 * Sslw (Zwieschutzweichentabelle)
	 */
	public String SslwDescriptionService_ViewName;

	/**
	 * Zwieschutzweichentabelle
	 */
	public String SslwDescriptionService_ViewTooltip;

	/**
	 * Art
	 */
	public String SslwTableView_Art;

	/**
	 * Art (Echt)
	 */
	public String SslwTableView_Art_Echt;

	/**
	 * Art (Eigen)
	 */
	public String SslwTableView_Art_Eigen;

	/**
	 * Bemerkung
	 */
	public String SslwTableView_Bemerkung;

	/**
	 * Ersatzschutz unmittelbar
	 */
	public String SslwTableView_Ersatzschutz_unmittelbar;

	/**
	 * Ersatzschutz unmittelbar Signal
	 */
	public String SslwTableView_Ersatzschutz_unmittelbar_Signal;

	/**
	 * Ersatzschutz unmittelbar Signal Bezeichnung
	 */
	public String SslwTableView_Ersatzschutz_unmittelbar_Signal_Bezeichnung_Sig;

	/**
	 * Rangierzielsperre
	 */
	public String SslwTableView_Ersatzschutz_unmittelbar_Signal_Rangierzielsperre;

	/**
	 * Ersatzschutz unmittelbar Weiche/Gleissperre
	 */
	public String SslwTableView_Ersatzschutz_unmittelbar_Weiche_Gleissperre;

	/**
	 * Ersatzschutz unmittelbar Weiche/Gleissperre Bezeichnung
	 */
	public String SslwTableView_Ersatzschutz_unmittelbar_Weiche_Gleissperre_Bezeichnung_W;

	/**
	 * Ersatzschutz unmittelbar Weiche/Gleissperre Lage
	 */
	public String SslwTableView_Ersatzschutz_unmittelbar_Weiche_Gleissperre_Lage;

	/**
	 * Ersatzschutz unmittelbar Weiche/Gleissperre Zwieschutz
	 */
	public String SslwTableView_Ersatzschutz_unmittelbar_Weiche_Gleissperre_Zwieschutz;

	/**
	 * Ersatzschutz Weitergabe
	 */
	public String SslwTableView_Ersatzschutz_Weitergabe;

	/**
	 * Ersatzschutz Weitergabe Weiche/Kreuzung
	 */
	public String SslwTableView_Ersatzschutz_Weitergabe_Weiche_Kreuzung;

	/**
	 * Ersatzschutz Weitergabe Weiche/Kreuzung Bezeichnung
	 */
	public String SslwTableView_Ersatzschutz_Weitergabe_Weiche_Kreuzung_Bezeichnung_W_Kr;

	/**
	 * Ersatzschutz Weitergabe Weiche/Kreuzung wie Fahrt über
	 */
	public String SslwTableView_Ersatzschutz_Weitergabe_Weiche_Kreuzung_wie_Fahrt_ueber;

	/**
	 * Ersatzschutz Weitergabe Zusätzlich bei EKW
	 */
	public String SslwTableView_Ersatzschutz_Weitergabe_Zusaetzlich_EKW;

	/**
	 * Ersatzschutz Weitergabe Zusätzlich bei EKW Bezeichnung
	 */
	public String SslwTableView_Ersatzschutz_Weitergabe_Zusaetzlich_EKW_Bezeichnung_W_Kr;

	/**
	 * Ersatzschutz Weitergabe Zusätzlich bei EKW wie Fahrt über
	 */
	public String SslwTableView_Ersatzschutz_Weitergabe_Zusaetzlich_EKW_wie_Fahrt_ueber;

	/**
	 * Zwieschutzweichentabelle (Sslw)
	 */
	public String SslwTableView_Heading;

	/**
	 * Nachlaufverhinderung
	 */
	public String SslwTableView_Nachlaufverhinderung;

	/**
	 * Flankenschutzraum
	 */
	public String SslwTableView_Schutzraumueberwachung;

	/**
	 * freigemeldet
	 */
	public String SslwTableView_Schutzraumueberwachung_freigemeldet;

	/**
	 * nicht freigemeldet
	 */
	public String SslwTableView_Schutzraumueberwachung_nicht_freigemeldet;

	/**
	 * Technischer Verzicht
	 */
	public String SslwTableView_Technischer_Verzicht;

	/**
	 * Verschluss
	 */
	public String SslwTableView_Verschluss;

	/**
	 * Weiche/Kreuzung in angeforderter Stellung
	 */
	public String SslwTableView_W_Kr_Stellung;

	/**
	 * Sslz (Zugstraßentabelle)
	 */
	public String SslzDescriptionService_ViewName;

	/**
	 * Zugstraßentabelle
	 */
	public String SslzDescriptionService_ViewTooltip;

	/**
	 * Abhängigkeiten
	 */
	public String SslzTableView_Abhaengigkeiten;

	/**
	 * Anrückverschluss
	 */
	public String SslzTableView_Abhaengigkeiten_Anrueckverschluss;

	/**
	 * Abhängiger BÜ
	 */
	public String SslzTableView_Abhaengigkeiten_BUE;

	/**
	 * Inselgleis
	 */
	public String SslzTableView_Abhaengigkeiten_Inselgleis;

	/**
	 * Nichthaltfallabschnitt
	 */
	public String SslzTableView_Abhaengigkeiten_NichthaltfallAbschnitt;

	/**
	 * Überwachte Ssp
	 */
	public String SslzTableView_Abhaengigkeiten_Ueberwachte_Ssp;

	/**
	 * 2. Haltfallkriterium
	 */
	public String SslzTableView_Abhaengigkeiten_Zweites_Haltfallkrit;

	/**
	 * Bemerkung
	 */
	public String SslzTableView_Bemerkung;

	/**
	 * Einstellung
	 */
	public String SslzTableView_Einstellung;

	/**
	 * Autom. Einstellung
	 */
	public String SslzTableView_Einstellung_Auto;

	/**
	 * F-Bedienung
	 */
	public String SslzTableView_Einstellung_F;

	/**
	 * Grundsatzangaben
	 */
	public String SslzTableView_Grundsatzangaben;

	/**
	 * Art
	 */
	public String SslzTableView_Grundsatzangaben_Art;

	/**
	 * Bezeichnung
	 */
	public String SslzTableView_Grundsatzangaben_Bezeichnung;

	/**
	 * D-Weg
	 */
	public String SslzTableView_Grundsatzangaben_Dweg;

	/**
	 * Bezeichnung
	 */
	public String SslzTableView_Grundsatzangaben_Dweg_Bezeichnung;

	/**
	 * Fahrweg
	 */
	public String SslzTableView_Grundsatzangaben_Fahrweg;

	/**
	 * Entscheidungsweiche mit Stellung
	 */
	public String SslzTableView_Grundsatzangaben_Fahrweg_EntschWeiche;

	/**
	 * Nr.
	 */
	public String SslzTableView_Grundsatzangaben_Fahrweg_Nr;

	/**
	 * Start
	 */
	public String SslzTableView_Grundsatzangaben_Fahrweg_Start;

	/**
	 * Ziel
	 */
	public String SslzTableView_Grundsatzangaben_Fahrweg_Ziel;

	/**
	 * Zugstraßentabelle (Sslz)
	 */
	public String SslzTableView_Heading;

	/**
	 * Signalisierung
	 */
	public String SslzTableView_Signalisierung;

	/**
	 * Geschwindigkeit am Startsignal
	 */
	public String SslzTableView_Signalisierung_GeschwAmStartsignal;

	/**
	 * Aufwertung bei Mwtfstr
	 */
	public String SslzTableView_Signalisierung_GeschwAmStartsignal_Aufwertung;

	/**
	 * Besonders
	 */
	public String SslzTableView_Signalisierung_GeschwAmStartsignal_Besonders;

	/**
	 * D-Weg
	 */
	public String SslzTableView_Signalisierung_GeschwAmStartsignal_DWeg;

	/**
	 * Fahrweg
	 */
	public String SslzTableView_Signalisierung_GeschwAmStartsignal_Fahrweg;

	/**
	 * VZG
	 */
	public String SslzTableView_Signalisierung_GeschwAmStartsignal_VZG;

	/**
	 * Zs 3(Zs 3 Ziel)
	 */
	public String SslzTableView_Signalisierung_GeschwAmStartsignal_Zs3;

	/**
	 * Im Fahrweg
	 */
	public String SslzTableView_Signalisierung_ImFahrweg;

	/**
	 * Kennlicht an
	 */
	public String SslzTableView_Signalisierung_ImFahrweg_Kennlicht;

	/**
	 * Vorsignalisierung Ziel
	 */
	public String SslzTableView_Signalisierung_ImFahrweg_Vorsignalisierung;

	/**
	 * Zs 3
	 */
	public String SslzTableView_Signalisierung_ImFahrweg_Zs3;

	/**
	 * Zs 6
	 */
	public String SslzTableView_Signalisierung_ImFahrweg_Zs6;

	/**
	 * Sonstiges am Startsignal
	 */
	public String SslzTableView_Signalisierung_Sonstiges;

	/**
	 * Zs 13
	 */
	public String SslzTableView_Signalisierung_Sonstiges_Zs13;

	/**
	 * Zs 2
	 */
	public String SslzTableView_Signalisierung_Sonstiges_Zs2;

	/**
	 * Zs 2 v
	 */
	public String SslzTableView_Signalisierung_Sonstiges_Zs2v;

	/**
	 * Zs 3 v
	 */
	public String SslzTableView_Signalisierung_Sonstiges_Zs3v;

	/**
	 * Zs 6
	 */
	public String SslzTableView_Signalisierung_Sonstiges_Zs6;

	/**
	 * Zl
	 */
	public String SslzTableView_Signalisierung_Sonstiges_Zusatzlicht;

	/**
	 * Ssvu (Übertragungswegtabelle)
	 */
	public String SsvuDescriptionService_ViewName;

	/**
	 * Übertragungswegtabelle
	 */
	public String SsvuDescriptionService_ViewTooltip;

	/**
	 * Bemerkung
	 */
	public String SsvuTableView_Bemerkung;

	/**
	 * Grundsatzangaben
	 */
	public String SsvuTableView_Grundsatzangaben;

	/**
	 * Grundsatzangaben nach
	 */
	public String SsvuTableView_Grundsatzangaben_nach;

	/**
	 * Grundsatzangaben Netzart
	 */
	public String SsvuTableView_Grundsatzangaben_Netzart;

	/**
	 * Grundsatzangaben Verwendung
	 */
	public String SsvuTableView_Grundsatzangaben_Verwendung;

	/**
	 * Grundsatzangaben von
	 */
	public String SsvuTableView_Grundsatzangaben_von;

	/**
	 * Übertragungswegtabelle (Ssvu)
	 */
	public String SsvuTableView_Heading;

	/**
	 * Technik
	 */
	public String SsvuTableView_Technik;

	/**
	 * Technik Bandbreite
	 */
	public String SsvuTableView_Technik_Bandbreite;

	/**
	 * Technik Netzart
	 */
	public String SsvuTableView_Technik_Netzart;

	/**
	 * Technik Schnittstelle
	 */
	public String SsvuTableView_Technik_Schnittstelle;

	/**
	 * Technik Technikart
	 */
	public String SsvuTableView_Technik_Technikart;

	/**
	 * Exportieren
	 */
	public String ToolboxTableView_Export;

	/**
	 * Export der Einzeltabelle
	 */
	public String ToolboxTableView_ExportTable;

	/**
	 * Zugstraßentabelle (alt)
	 */
	public String ZstTableView_Heading;

	/**
	 * Selbststellbetrieb/Zuglenkung SB / ZL
	 */
	public String ZstTableView_HeadingAutomaticRouteSetting;

	/**
	 * Ssp in der Fahrstraße - verschlossen und überwacht
	 */
	public String ZstTableView_HeadingKeyLockLockedGuarded;

	/**
	 * Zugstraße in nicht überspannte Bereiche
	 */
	public String ZstTableView_HeadingNotSpannedSection;

	/**
	 * Bemerkungen
	 */
	public String ZstTableView_HeadingRemarks;

	/**
	 * Zugstraße
	 */
	public String ZstTableView_HeadingRunningTrack;

	/**
	 * Start / Ziel
	 */
	public String ZstTableView_HeadingRunningTrackOriginDestination;

	/**
	 * Durchrutschweg
	 */
	public String ZstTableView_HeadingRunningTrackSlipDistance;

	/**
	 * Art 1)
	 */
	public String ZstTableView_HeadingRunningTrackType;

	/**
	 * Haltstellungsverhinderung
	 */
	public String ZstTableView_HeadingTurnStopPrevention;

	/**
	 * Tabellenübersicht
	 */
	public String TableOverviewDescriptionService_ViewName;

	/**
	 * Es wurden noch nicht alle Tabellen auf Fehler überprüft, diese Ansicht
	 * ist möglicherweise nicht vollständig.
	 */
	public String TableOverviewPart_CompletenessHint;

	/**
	 * Tabellen
	 */
	public String TableOverviewPart_TableSectionHeader;

	/**
	 * Nicht überprüft:
	 */
	public String TableOverviewPart_MissingTablesDesc;

	/**
	 * Überprüfen
	 */
	public String TableOverviewPart_CalculateMissing;

	/**
	 * Enthalten Fehler:
	 */
	public String TableOverviewPart_WithErrorsDesc;

	/**
	 * <keine>
	 */
	public String TableOverviewPart_EmptyListText;

	/**
	 * Alle öffnen
	 */
	public String TableOverviewPart_OpenAllWithErrors;

	/**
	 * Führendes Objekt
	 */
	public String TableErrorTableColumns_LeadingObject;

	/**
	 * Quelle
	 */
	public String TableErrorTableColumns_Source;

	/**
	 * Fehlertext
	 */
	public String TableErrorTableColumns_Message;
}
